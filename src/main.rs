use std::io;
use std::path::Path;
use env_logger;
use wozpack::{self,WozFile};


fn main() -> io::Result<()> {
    // RUST_LOG="debug" cargo run

    env_logger::init();

    // WozFile::read(Path::new("/mnt/data2/roms/Apple/choplifter.do.woz"));
    // WozFile::read(Path::new("LOWTECH.WOZ"));
    // wozpack::load_disk(Path::new("CHP.dsk"));
    wozpack::load_disk(Path::new("cracking.woz"));
    Ok(())
}
