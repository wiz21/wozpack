use std::fs::File;
use std::path::Path;
use std::str::{self};
use std::fmt::{Display, Formatter, Error};
use std::io::{Cursor, Read, Write};
use std::time::Duration;
use std::usize;
use std::cmp::min;
use log::{debug, error, info, trace, warn, LevelFilter};


use crc::{Crc, CRC_16_IBM_SDLC};
const X25: Crc<u16> = Crc::<u16>::new(&CRC_16_IBM_SDLC);
use bitstream_io::{BigEndian, BitReader, BitRead, BitWriter, BitWrite, Endianness};

struct NumVec(Vec<u8>);

// DSK file stuff
const DSK_VOLUME:u8 = 254; // FIXME With that value Conan (and others) works. Why is it important ?
const SECTOR_SIZE:usize = 256;
const SECTORS_PER_TRACK:usize = 16;
//const TRACK_SIZE:usize = SECTOR_SIZE * SECTORS_PER_TRACK;
const TRACKS_PER_FLOPPY:usize = 35;
const SKEWING_TABLE: [u8; 16] = [0,7,14,6,13,5,12,4,11,3,10,2,9,1,8,15];

// How many CPU cycles are necessary for the LSS to resync
// with read pulses. It should be the maximum number
// of cycles to go from one sync bits to another (provided those exist...)
// For example, PrinceOfPersia is 768 nibbles per sector. 8 bits per nibble, 4 cycles per bit.
// 768*8*4 ~= 25000 cycles
const CYCLES_TO_RESYNC_LSS: u64 = 768*8*4;

const NB_BITS_IN_EMPTY_TRACK: usize = 383*16*8; // Approx 200_000 cycles / 4 cycles per bit

// Number of tracks where the read head can go
// (equal or greater to the number of track on any floppy disk)
const DRIVE_SUB_TRACKS: usize = 36*4;
/*
 * Normal byte (lower six bits only) -> disk byte translation table.
 */
static BYTE_TRANSLATION: [u8; 64] = [
    0x96, 0x97, 0x9a, 0x9b, 0x9d, 0x9e, 0x9f, 0xa6,
    0xa7, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb2, 0xb3,
    0xb4, 0xb5, 0xb6, 0xb7, 0xb9, 0xba, 0xbb, 0xbc,
    0xbd, 0xbe, 0xbf, 0xcb, 0xcd, 0xce, 0xcf, 0xd3,
    0xd6, 0xd7, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde,
    0xdf, 0xe5, 0xe6, 0xe7, 0xe9, 0xea, 0xeb, 0xec,
    0xed, 0xee, 0xef, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6,
    0xf7, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff ];



fn nibblize_sector(vol: u8, trk: u8, sector: u8, bytes: &[u8], bit_writer: &mut BitWriter<&mut Vec<u8>,BigEndian> ) {

    let chksum: u8 = vol^trk^sector;
    let address_block: [u8; 8] = [
        ((vol >> 1) | 0xaa), (vol | 0xaa),
        ((trk >> 1) | 0xaa), (trk | 0xaa),
        ((sector >> 1) | 0xaa), (sector | 0xaa),
        ((chksum >> 1) | 0xaa), (chksum | 0xaa)
    ];

    let mut sector_buffer:Vec<u8> = Vec::new();
    sector_buffer.extend_from_slice(bytes);
    sector_buffer.extend_from_slice(&[0 as u8,0]);

    let mut prev_value = 0;
    let mut value = 0;
    let mut data_block_1 = [0u8; 86];
    for i in 0..86 // 86*3 = 258
    {
        value  = (sector_buffer[i] & 0x01) << 1;
        value |= (sector_buffer[i] & 0x02) >> 1;
        value |= (sector_buffer[i + 86] & 0x01) << 3;
        value |= (sector_buffer[i + 86] & 0x02) << 1;
        value |= (sector_buffer[i + 172] & 0x01) << 5;
        value |= (sector_buffer[i + 172] & 0x02) << 3;
        data_block_1[i] = BYTE_TRANSLATION[(value ^ prev_value) as usize];
        prev_value = value;
    }

    let mut data_block_2 = [0 as u8; SECTOR_SIZE];
    for i in 0..SECTOR_SIZE
    {
        value = sector_buffer[i] >> 2;
        data_block_2[i] = BYTE_TRANSLATION[(value ^ prev_value) as usize];
        prev_value = value;
    }

    let checksum: u8 = BYTE_TRANSLATION[value as usize];

    if sector == 0 {
	// Beginning of track

	// Sync
	for _i in 0..16 {
	    bit_writer.write(8,0b11111111 as u8).expect("success");
	    bit_writer.write(2,0b00 as u8).expect("success");
	}
    }

    bit_writer.write_bytes(&([0xd5, 0xaa, 0x96])).expect("success");
    bit_writer.write_bytes(&address_block).expect("success");
    bit_writer.write_bytes(&([0xDE, 0xAA, 0xEB])).expect("success");

    // Sync
    for _i in 0..7 {
	bit_writer.write(8,0b11111111 as u8).expect("success");
	bit_writer.write(2,0b00 as u8).expect("success");
    }

    bit_writer.write_bytes(&([0xd5, 0xaa, 0xad])).expect("success");
    bit_writer.write_bytes(&data_block_1).expect("success");
    bit_writer.write_bytes(&data_block_2).expect("success");
    bit_writer.write(8, checksum).expect("success");
    bit_writer.write_bytes(&([0xde, 0xaa, 0xeb])).expect("success");

    // Sync
    for _i in 0..16 {
	bit_writer.write(8,0b11111111 as u8).expect("success");
	bit_writer.write(2,0b00 as u8).expect("success");
    }

}




const CLR: u8 = 0;   // CLEAR DATA REGISTER
const NOP: u8 = 8;   // NO OPERATION
const SL0: u8 = 9;   // SHIFT ZERO LEFT INTO DATA REGISTER
// const SR:  u8 = 0xA; // SHIFT WRITE PROTECT SIGNAL RIGHT INTO DATA REGISTER
// const LD:  u8 = 0xB; // LOAD DATA REGISTER FROM DATA BUS
const SL1: u8 = 0xD; // SHIFT ONE LEFT INTO DATA REGISTER

const SEQ_ROM: [u8; 256] = [
    0x18, 0x18, 0x18, 0x18, 0x0A, 0x0A, 0x0A, 0x0A, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18,
    0x2D, 0x38, 0x2D, 0x38, 0x0A, 0x0A, 0x0A, 0x0A, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28,
    0x38, 0x28, 0xD8, 0x08, 0x0A, 0x0A, 0x0A, 0x0A, 0x39, 0x39, 0x39, 0x39, 0x3B, 0x3B, 0x3B, 0x3B,
    0x48, 0x48, 0xD8, 0x48, 0x0A, 0x0A, 0x0A, 0x0A, 0x48, 0x48, 0x48, 0x48, 0x48, 0x48, 0x48, 0x48,
    0x58, 0x58, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0x58, 0x58, 0x58, 0x58, 0x58, 0x58, 0x58, 0x58,
    0x68, 0x68, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68,
    0x78, 0x78, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0x78, 0x78, 0x78, 0x78, 0x78, 0x78, 0x78, 0x78,
    0x88, 0x88, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0x08, 0x88, 0x08, 0x88, 0x08, 0x88, 0x08, 0x88,
    0x98, 0x98, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0x98, 0x98, 0x98, 0x98, 0x98, 0x98, 0x98, 0x98,
    0x29, 0xA8, 0xD8, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0xA8, 0xA8, 0xA8, 0xA8, 0xA8, 0xA8, 0xA8, 0xA8,
    0xBD, 0xB8, 0xCD, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0xB9, 0xB9, 0xB9, 0xB9, 0xBB, 0xBB, 0xBB, 0xBB,
    0x59, 0xC8, 0xD9, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0xC8, 0xC8, 0xC8, 0xC8, 0xC8, 0xC8, 0xC8, 0xC8,
    0xD9, 0xA0, 0xD9, 0xD8, 0x0A, 0x0A, 0x0A, 0x0A, 0xD8, 0xD8, 0xD8, 0xD8, 0xD8, 0xD8, 0xD8, 0xD8,
    0x08, 0xE8, 0xD8, 0xE8, 0x0A, 0x0A, 0x0A, 0x0A, 0xE8, 0xE8, 0xE8, 0xE8, 0xE8, 0xE8, 0xE8, 0xE8,
    0xFD, 0xF8, 0xFD, 0xF8, 0x0A, 0x0A, 0x0A, 0x0A, 0xF8, 0xF8, 0xF8, 0xF8, 0xF8, 0xF8, 0xF8, 0xF8,
    0x4D, 0xE0, 0xDD, 0xE0, 0x0A, 0x0A, 0x0A, 0x0A, 0x88, 0x08, 0x88, 0x08, 0x88, 0x08, 0x88, 0x08];


struct CountingBitWriter {
    bit_writer: BitWriter<Vec<u8>, BigEndian>
}

impl CountingBitWriter {
    fn new() -> CountingBitWriter {
	CountingBitWriter {
	    bit_writer: BitWriter::endian(Vec::new(), BigEndian)
	}
    }
}

struct LSS {
    state: u8,
    data_register: u8
}

impl LSS {
    fn new() -> LSS {
	LSS {
	    state: 0,
	    data_register: 0
	}
    }

    fn seq_rom_value(&self, state: u8, rw_switch: u8, sl_switch: u8, read_pulse: u8, qa: u8) -> u8 {
        let r = (state & 0xF0) | (rw_switch << 3) | (sl_switch << 2) | (read_pulse << 1) | qa;
        SEQ_ROM[r as usize]
    }

    fn next_state(&mut self, rw_switch:u8, sl_switch:u8, read_pulse:u8) {
	let qa = (self.data_register & 0x80) >> 7;
        self.state = self.seq_rom_value(self.state, rw_switch, sl_switch, read_pulse, qa)
    }

    fn apply_command(&mut self) {
	match self.state & 0xF {
	    CLR => self.data_register = 0,
	    SL0 => self.data_register = (self.data_register << 1) + 0,
	    SL1 => self.data_register = (self.data_register << 1) + 1,
	    NOP => (),
	    _ => panic!("Unsupported LSS command")
	}
    }

    pub fn tick(&mut self, rw_switch:u8, sl_switch:u8, read_pulse:bool) {
	self.next_state(rw_switch, sl_switch, if read_pulse {1} else {0});
	self.apply_command();
    }
}

pub struct Controller {
    woz_file: WozFile,
    current_subtrack: usize,
    destination_subtrack: Option<usize>, // None = no track move currently ongoing
    destination_subtrack_cycle_stop: u64,
    pub track_bit_index: usize,
    last_cycle: u64,
    sub_cycles_before_read_pulse:u64,
    last_read_pulse: bool,
    is_spinning: bool,
    lss : LSS,
    last_phase: usize
}


const MICROSEC_PER_READ_PULSE: u64 = 4;
const EMPTY_SUB_TRACK: usize = 0xFF;
const SUB_CYCLES_PER_CYCLE: u64 = 8; // Must be divisible by two (for LSS frequency derivation)


impl Controller {
    pub fn new() -> Controller {
	Controller {
	    woz_file : WozFile::new(),
	    current_subtrack: 0,
	    destination_subtrack: None,
	    destination_subtrack_cycle_stop: 0,
	    track_bit_index: 0,
	    last_cycle: 0,
	    sub_cycles_before_read_pulse: 0,
	    last_read_pulse: false,
	    is_spinning: true,
	    lss: LSS::new(),
	    last_phase: 0
	}
    }

    pub fn current_subtrack(&self) -> usize {
	self.current_subtrack
    }

    pub fn set_phase(&mut self, cycle:u64, phase:usize) {
	//self.time_passes_up_to(cycle);

	// Quarter of track to half track
	// 1 half track == one motor step
	// => 2 motor steps == one track.
	let pos = (self.current_subtrack >> 1) % 4;

	let direction = if
	    (phase == 1 && self.last_phase == 0 && pos == 0)
	    || (phase == 2 && self.last_phase == 1 && pos == 1)
	    || (phase == 3 && self.last_phase == 2 && pos == 2)
	    || (phase == 0 && self.last_phase == 3 && pos == 3) {
		2
	    } else if
	    (phase == 0 && self.last_phase == 1 && pos == 1)
	    || (phase == 1 && self.last_phase == 2 && pos == 2)
	    || (phase == 2 && self.last_phase == 3 && pos == 3)
	    || (phase == 3 && self.last_phase == 0 && pos == 0) {
		-2
	    } else {
		0
	    };


	let old_subtrack = self.current_subtrack;
	if direction != 0 {
	    self.begin_head_move(cycle, direction)
	}
	debug!("Trk={}, old phase: {}, new phase: {} dir={} --> trk={}",
	       (old_subtrack as f32) / 4.0,
	       self.last_phase, phase, direction,
	       (self.current_subtrack as f32) / 4.0);

	self.last_phase = phase;
    }

    pub fn set_woz(&mut self, woz: WozFile) {
	self.woz_file = woz;
    }


    fn is_read_head_travelling(&self) -> bool {
	self.destination_subtrack.is_some()
    }

    fn time_passes_up_to(&mut self, cycle_to: u64) {
	assert!(cycle_to > self.last_cycle);

	if self.is_read_head_travelling() {
	    if self.destination_subtrack_cycle_stop < cycle_to {
		// The head will reach its destination before
		// the cycle we're interested in

		self.time_passes_up_to(self.destination_subtrack_cycle_stop);
		self.end_head_move();
	    }
	}


	let cycles_to_go = cycle_to - self.last_cycle;

	// Time passes while motor, read head moves remain constant.

	let motor_is_on = true;
	let head_is_on_subtrack = !self.is_read_head_travelling();

	trace!("Time passes up to {} for {} cycles. motor_is_on:{}, head in place:{}. Subtrk: {}->{:02X}. BitIndex:{} ",
	       cycle_to, cycles_to_go, motor_is_on, head_is_on_subtrack,
	       self.current_subtrack,
	       self.woz_file.sub_track_index[self.current_subtrack],
	       self.track_bit_index
	);

	if motor_is_on {

	    let maxed_cycles_to_go = if cycles_to_go > CYCLES_TO_RESYNC_LSS {
		// The last read from the LSS was very long ago so we have
		// many cycles to go.  We assume the LSS is screwed and we
		// can safely say it's safe to start from wherever we are
		// now, discarding history.

		// So we resume operations at last position plus threshold.
		// From there on, we'll go on with regular LSS processing
		// up to last position plus what's left of cycles_to_go.

		self.fast_forward_floppy(cycles_to_go - CYCLES_TO_RESYNC_LSS);
		CYCLES_TO_RESYNC_LSS
	    } else {
		cycles_to_go
	    };

	    if head_is_on_subtrack {
		self.run_LSS_on_floppy(maxed_cycles_to_go);
	    } else {
		self.run_LSS_without_floppy(maxed_cycles_to_go);
	    }
	} else {
	    if head_is_on_subtrack {
		self.run_LSS_without_floppy(cycles_to_go);
	    } else {
		self.run_LSS_without_floppy(cycles_to_go);
	    }
	}

	self.last_cycle = cycle_to;
    }

    pub fn begin_head_move(&mut self, cycle: u64, direction: isize) {
	//assert!(direction == 1 || direction == -1);
	self.time_passes_up_to(cycle);

	// The head is not already travelling to the next track
	// and the request direction is physically possible.
	if !self.is_read_head_travelling() {
	    if (direction > 0 && self.current_subtrack < DRIVE_SUB_TRACKS - 1)
		|| (direction < 0 && self.current_subtrack > 0) {

		    // Delayed move: FIXME (what happens if one moves
		    // the read head several times without ever reading
		    // the data register ?

		    // self.destination_subtrack = Some(((self.current_subtrack as isize) + direction) as usize);
		    // self.destination_subtrack_cycle_stop = cycle + 1000;
		    // trace!("Head set to move to subtrack {}", self.destination_subtrack.unwrap());

		    // Instantaneous head move. For debugging.
		    self.current_subtrack = ((self.current_subtrack as isize) + direction) as usize;

		} else {
		    warn!("Requesting head to move outside limit from {} to {}",
			  self.current_subtrack,
			  self.current_subtrack as isize + direction);
		}
	} else {
	    warn!("Requesting head to move from {} to {} while it's already moving",
		  self.current_subtrack,
		  self.current_subtrack as isize + direction);
	}
    }

    fn end_head_move(&mut self) {

	// Land on the next sub track
	// FIXME Recvompute this (attention aux tracks vides !)

	if self.is_read_head_travelling() {
	    self.current_subtrack = self.destination_subtrack.unwrap();

	    // Stop the head
	    self.destination_subtrack = None;
	    // Start reading again
	    self.sub_cycles_before_read_pulse = 0;
	    debug!("End at track {} -> {}", self.current_subtrack, self.current_subtrack_index());
	} else {
	    warn!("Requesting head to stop while it's not moving");
	}

    }


    fn run_LSS_on_floppy(&mut self, mut cycles_to_go: u64) {
	assert!(cycles_to_go > 0, "Don't call this if there's nothing to do");

	// From Woz spec 2.1: "A single tick is 125
	// nanoseconds. Therefore the normal 4 microsecond spacing
	// between sequential 1 bits is represented by approximately
	// 32 ticks."

	let sub_cycles = cycles_to_go*SUB_CYCLES_PER_CYCLE; // 1/8th of a cycle

	trace!("Loop for {} cycles, sub_cycles:{}, sub_cycles_before_read_pulse:{}",
	       cycles_to_go,
	       sub_cycles,
	       self.sub_cycles_before_read_pulse);

	for i in 0..sub_cycles {
	    if self.sub_cycles_before_read_pulse == 0 {
		self.sub_cycles_before_read_pulse = SUB_CYCLES_PER_CYCLE * MICROSEC_PER_READ_PULSE;
		self.last_read_pulse = self.rotate_floppy();


		if cycles_to_go < 50 {
		    if self.current_subtrack_index() != EMPTY_SUB_TRACK {
			let track_data = &self.woz_file.track_data[self.current_subtrack_index()];
			trace!("rotate_floppy {}/{} bits. Cur. byte ofs:{} = {:08b} ${:02X}, mask={:08b}, {:02X}",
			       self.track_bit_index,
			       track_data.nb_bits,
			       self.track_bit_index / 8,
			       track_data.track_bits[self.track_bit_index / 8],
			       track_data.track_bits[self.track_bit_index / 8],
			       128 >> self.track_bit_index % 8,
			       self.lss.data_register);
		    } else {
			trace!("rotate_floppy (empty track) {} bits. DataReg:{:02X}",
			       self.track_bit_index,
			       self.lss.data_register);
		    }
		}


	    } else {
		self.sub_cycles_before_read_pulse -= 1;
	    };

	    if i % SUB_CYCLES_PER_CYCLE == 0 {
		self.lss.tick(0,0,self.last_read_pulse);
		self.last_read_pulse = false;
	    } else if i % SUB_CYCLES_PER_CYCLE == SUB_CYCLES_PER_CYCLE / 2 {
		self.lss.tick(0,0,false);
	    }
	}
    }

    fn run_LSS_without_floppy(&mut self, cycles_to_go: u64) {
	// Run for cycles to go when the read head
	// is travelling or motor is stopped (hence not able to read bits)
	// or if the subtrack is empty

	for _i in 0..cycles_to_go {
	    self.lss.tick(0,0,false);
	    self.last_read_pulse = false;
	}
    }

    pub fn data_register(&mut self, cycle: u64) -> u8 {
	/*
	There are several things to take into account:
	- the drive motor may be on or off
	- the read head may be moving or not
	- the subtrack may be empty or not
	 */

	assert!(cycle > self.last_cycle, "Can't go back in time");

	self.time_passes_up_to(cycle);
	self.lss.data_register
    }

    fn is_current_subtrack_empty(&self) -> bool {
	self.current_subtrack_index() == EMPTY_SUB_TRACK
    }

    fn current_subtrack_index(&self) -> usize {
	// We accomodate for the fact that the read head may be
	// outside the range of tracks available on the floppy disk.

	if self.current_subtrack < self.woz_file.sub_track_index.len() {
	    self.woz_file.sub_track_index[self.current_subtrack]
	} else {
	    EMPTY_SUB_TRACK
	}
    }

    fn rotate_floppy(&mut self) -> bool {
	if self.current_subtrack_index() != EMPTY_SUB_TRACK {
	    let track_data = &self.woz_file.track_data[self.current_subtrack_index()];

	    if self.track_bit_index < track_data.nb_bits - 1 {
		self.track_bit_index += 1;
	    } else {
		self.track_bit_index = 0
	    }

	    if track_data.track_bits[self.track_bit_index / 8] & (128 >> self.track_bit_index % 8) > 0 {
		true
	    } else {
		false
	    }
	} else {
	    // Empty subtrack
	    if self.track_bit_index < NB_BITS_IN_EMPTY_TRACK {
		self.track_bit_index += 1;
	    } else {
		self.track_bit_index = 0
	    }

	    false
	}
    }


    fn fast_forward_floppy(&mut self, cycles_to_go: u64) {
	// Compute an approximate position in the bit stream
	// provided we don't actually read the stream. We just let time
	// pass.
	// This happens when the user wants to read the disk after not
	// reading it for a long time.
	// The exact way to do that would be to simulate the disk rotation,
	// LSS, data register, etc. for all the "cycles_to_go" but this
	// could potentially run for millions of cycles if the user remained
	// inactive for a while. So we make this shortcut.

	// Where the fast forward will end.
	// Note we don't consume all the cycles to go, we
	// leave some cyles left for more detailed run afterwards.
	// We move to a place before the en cycle so that we'll
	// be able to let the LSS synchronize a bit.

	let end_cycle: u64 = self.last_cycle + cycles_to_go;

	// Now things are a bit tricky.
	// We count the time since the start of the floppy rotation.
	let rotation_start_cycle: u64 = 0; // FIXME Assumed 0 for now.
	// But when the floppy started it had a given "offset" to the
	// "beginning" of the disk. This offset is measured in cycles
	// because if we were using bit position in the bitstream,
	// that position would depend on the bit stream length which
	// vary from track to track and doesn't take into account
	// the fact that there are "empty" tracks.
	let rotation_start_cycle_offset: u64 = 0; // FIXME Assumed 0 for now.

	// The floppy rotates at a speed of *about* rotation every 200000 CPU cycles.
	// When reading WOZ files, the number of bits in track indicate the
	// the number of bits that cover a *full* floppy rotation. That is
	// the time to make a full rotation may be 200000 cycles but the information
	// density is different. So, in the end, it's better to measure the
	// passage of time in cycles instead of bits.

	// FIXME The division below has a remainder which is not taken into account.

	let bits_in_track = if self.current_subtrack_index() != EMPTY_SUB_TRACK {
	    self.woz_file.track_data[self.current_subtrack_index()].nb_bits }
	else {
	    NB_BITS_IN_EMPTY_TRACK
	};

	self.track_bit_index = (((rotation_start_cycle_offset + (end_cycle - rotation_start_cycle)) / MICROSEC_PER_READ_PULSE) as usize) % bits_in_track;

    }

}


impl Display for NumVec {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let mut comma_separated = String::new();

        for num in &self.0 {
            comma_separated.push_str(&format!("{:02X}",num));
            comma_separated.push_str(" ");
        }

        write!(f, "{}", comma_separated)
    }
}

fn bits_to_cycle_data_register_values( nb_bits: usize, cursor:&mut Cursor<&[u8]>) -> Vec<u8>{
    //let mut cursor = Cursor::new(&buffer[block_start*BLOCK_SIZE..(block_start+nb_blocks)*BLOCK_SIZE]);
    let mut bit_reader = BitReader::endian(cursor, BigEndian);
    let mut lss = LSS::new();
    let mut data_regs: Vec<u8> = Vec::new();

    let mut i = 0;
    while i < nb_bits {
	// ---------- usec = 0.0 ----------
	// Floppy bit : one every 4 usec, sustained for 2usec
	let mut bit: bool = bit_reader.read_bit().unwrap();
	i += 1;
	// LSS cycle : 2 per usec
	lss.tick(0,0,bit);
	bit = false; // LSS clears the read pulse when it reads it
	// CPU cycle : 1 per usec
	data_regs.push(lss.data_register);
	// ---------- usec = 0.5 ----------
	lss.tick(0,0,bit);
	// ---------- usec = 1.0 ----------
	lss.tick(0,0,bit);
	data_regs.push(lss.data_register);
	// ---------- usec = 1.5 ----------
	lss.tick(0,0,bit);
	let bit: bool = false;
	// ---------- usec = 2.0 ----------
	lss.tick(0,0,bit);
	data_regs.push(lss.data_register);
	// ---------- usec = 2.5 ----------
	lss.tick(0,0,bit);
	// ---------- usec = 3.0 ----------
	lss.tick(0,0,bit);
	data_regs.push(lss.data_register);
	// ---------- usec = 3.5 ----------
	lss.tick(0,0,bit);
    }

    if nb_bits % 8 != 0 {
	for _i in 0..(8-(nb_bits%8)) {
	    lss.tick(0,0,false);
	    data_regs.push(lss.data_register);
	}
    }

    return data_regs;
}



const BLOCK_SIZE: usize = 512;

pub struct TrackData {
    track_bits: Vec<u8>,
    nb_bits: usize
}

impl TrackData {
    fn new(track_bits: Vec<u8>, nb_bits: usize) -> TrackData {
	TrackData {
	    track_bits,
	    nb_bits
	}
    }
}

pub struct WozFile {
    version: usize,
    optimal_bit_timing: usize,

    // Links sub track number ( 0,0.25,0.5,0.75,1,1.25,...)
    // to track data
    sub_track_index: Vec<usize>,
    track_data: Vec<TrackData>,
    pub data_register_values: Vec<Vec<u8>>

}

impl WozFile {
    pub fn new() -> WozFile {
	WozFile {
	    version: 2,
	    optimal_bit_timing: 32, // times 0.125 nanosec => 4 usec
	    track_data: vec![],
	    sub_track_index: vec![],
	    data_register_values: vec![]
	}
    }

    pub fn data_register(&self, cycle: u64, track: usize, subtrack: usize) -> u8 {
	//println!("Track {}", track);
	let ndx = self.sub_track_index[track*4 + subtrack];
	trace!("track:{} subtrack:{} ndx:{}", track, subtrack, ndx);
	let track_data = &self.data_register_values[ndx];
	let cycle_ndx = (cycle % (track_data.len() as u64)) as usize;
	track_data[cycle_ndx]
    }

    pub fn read(file_path: &Path) -> WozFile {
	let mut woz_file = WozFile::new();
	let mut f = File::open(file_path).expect("open");
	let mut buffer:Vec<u8> = Vec::new();

	// read the whole file
	f.read_to_end(&mut buffer).expect("read");

	X25.checksum(buffer.as_slice());

	let woz_ident = str::from_utf8(&buffer[0..4]);
	match woz_ident.unwrap() {
	    "WOZ1" => woz_file.version = 1,
	    "WOZ2" => woz_file.version = 2,
	    _ => panic!("Unsupported WOZ version : '{}'", woz_ident.unwrap())
	}
	debug!("{}! {} bytes", woz_ident.unwrap(), buffer.len());

	let mut chunk_ptr = 12;

	// INFO chunk ---------------------------------------------------------

	let chunk_size = (u32::from_le_bytes(buffer[(chunk_ptr+4)..(chunk_ptr+4+4)].try_into().unwrap())) as usize;

	debug!("Type:{} Size:{} !",
		 str::from_utf8(&buffer[chunk_ptr..(chunk_ptr+4)]).unwrap(),
		 chunk_size);
	chunk_ptr += 8;
	woz_file.version = buffer[chunk_ptr] as usize;
	debug!("Version:{} DiskType:{}, Write Protected:{}, Synchronised:{}, cleaned:{}, creator:{}!",
		 buffer[chunk_ptr],
		 buffer[chunk_ptr+1],
		 buffer[chunk_ptr+2],
		 buffer[chunk_ptr+3],
		 buffer[chunk_ptr+4],
		 str::from_utf8(&buffer[(chunk_ptr+5)..(chunk_ptr+5+32)]).unwrap());

	if woz_file.version >= 2 {
	    woz_file.optimal_bit_timing = buffer[chunk_ptr+39] as usize;
	    assert!(woz_file.optimal_bit_timing == 32, "Unsupported optimal bit timing : {}", woz_file.optimal_bit_timing);
	    debug!("Disk sides:{} Boot sector format:{}, Optimal bit timing:{}μs, Compatible hardware:{:016b}",
		     buffer[chunk_ptr+37],
		     buffer[chunk_ptr+38],
		     Duration::from_secs_f32((buffer[chunk_ptr+39] as f32) * 125e-9).as_micros(),
		     u16::from_le_bytes(buffer[(chunk_ptr+40)..(chunk_ptr+40+2)].try_into().unwrap()));

	    debug!("required ram:{} Kb largest track:{} blocks (512 bytes).",
		     u16::from_le_bytes(buffer[(chunk_ptr+42)..(chunk_ptr+42+2)].try_into().unwrap()),
		     u16::from_le_bytes(buffer[(chunk_ptr+44)..(chunk_ptr+44+2)].try_into().unwrap()));

	}
	chunk_ptr += chunk_size;

	// TMAP chunk ---------------------------------------------------------

	debug!("Type:{} Size:{} !",
		 str::from_utf8(&buffer[chunk_ptr..(chunk_ptr+4)]).unwrap(),
		 u32::from_le_bytes(buffer[(chunk_ptr+4)..(chunk_ptr+4+4)].try_into().unwrap()));
	chunk_ptr += 8;

	let mut track_max_index: usize = 0;
	for i in 0..DRIVE_SUB_TRACKS {
	    let track_index = buffer[chunk_ptr+i] as usize;

	    debug!("Trk {}, 1/4 trk: {}/4, {}",i/4, i%4,
		     if track_index == EMPTY_SUB_TRACK {
			 String::from("Unused")
		     } else {
			 format!("{}", buffer[chunk_ptr+i])
		     });
	    if track_index != EMPTY_SUB_TRACK && track_index > track_max_index {
		track_max_index = track_index;
	    }
	    woz_file.sub_track_index.push(buffer[chunk_ptr+i] as usize);
	}

	chunk_ptr += 160;

	// TRKS chunk ---------------------------------------------------------

	debug!("Type:'{}' Size:{} !",
		 str::from_utf8(&buffer[chunk_ptr..(chunk_ptr+4)]).unwrap(),
		 u32::from_le_bytes(buffer[(chunk_ptr+4)..(chunk_ptr+4+4)].try_into().unwrap()));
	chunk_ptr += 8;

	if woz_file.version == 1 {
	    for track_index in 0..track_max_index+1 {
		let track_data_start = chunk_ptr + track_index*6656;
		let track_data_end = chunk_ptr + (track_index+1)*6656;
		let track_data = &buffer[track_data_start..track_data_end];
		let nb_bits = (u16::from_le_bytes(track_data[6648..6648+2].try_into().unwrap())) as usize;
		let nb_bytes = (u16::from_le_bytes(track_data[6646..6646+2].try_into().unwrap())) as usize;

		woz_file.track_data.push(TrackData::new(track_data.into(), nb_bits));

		let mut cursor = Cursor::new(&track_data[0..6646]);
		let data_regs = bits_to_cycle_data_register_values( nb_bits, &mut cursor);
		debug!("Track: {:02}, {} bits ({} bytes /{} bytes), data regs: {}",
			 track_index,
			 nb_bits,
			 nb_bits / 8,
			 nb_bytes,
			 data_regs.len());
		woz_file.data_register_values.push(data_regs);
	    }
	}

	if woz_file.version == 2 {
	    for sub_trk in 0..160 {
		let ofs = chunk_ptr + sub_trk*8;

		let block_start: usize = u16::from_le_bytes(buffer[(ofs)..(ofs+2)].try_into().unwrap()) as usize;
		let nb_blocks = u16::from_le_bytes(buffer[(ofs+2)..(ofs+4)].try_into().unwrap()) as usize;
		let nb_bits = u32::from_le_bytes(buffer[(ofs+4)..(ofs+8)].try_into().unwrap()) as usize;
		let mut data_regs: Vec<u8> = Vec::new();

		debug!("About to read {} 512-bytes blocks, {} bits ({} bytes), {}", nb_blocks, nb_bits, nb_bits/8, ofs);
		if nb_blocks > 0 {

		    // Block start is relative to the *beginning of the file*
		    let mut cursor = Cursor::new(&buffer[block_start*BLOCK_SIZE..(block_start+nb_blocks)*BLOCK_SIZE]);
		    data_regs = bits_to_cycle_data_register_values( nb_bits, &mut cursor);

		    let track_data = &buffer[block_start*BLOCK_SIZE..(block_start+nb_blocks)*BLOCK_SIZE];
		    woz_file.track_data.push(TrackData::new(track_data.into(), nb_bits));

		    debug!("Track: {:02}, start block:{:04}, {} blocks, {} bits ({} bytes /{} bytes), data regs: {}",
			   sub_trk,
			   block_start,
			   nb_blocks,
			   nb_bits,
			   nb_bits / 8,
			   (nb_blocks as usize) * BLOCK_SIZE,
			   data_regs.len());
		}

		woz_file.data_register_values.push(data_regs);
	    } // for loop
	}
	woz_file
    }



    pub fn read_dsk(file_path: &Path) -> WozFile {
	// The general idea is : get the DSK, make a WOZ out of it.

	debug!("DSK import: Reading {} ",file_path.to_str().unwrap());

	let mut woz_file = WozFile::new();
	let mut f = File::open(file_path).expect("open should work");
	let mut buffer:Vec<u8> = Vec::new();
	// read the whole file
	f.read_to_end(&mut buffer).expect("read");


	for track_index in 0..TRACKS_PER_FLOPPY {

	    let mut track_bits: Vec<u8> = Vec::new();
	    let mut bit_writer = BitWriter::endian(track_bits.by_ref(), BigEndian);

	    for sector in 0..SECTORS_PER_TRACK {
		let rs = SKEWING_TABLE[sector] as usize;
		let sector_start = track_index*SECTORS_PER_TRACK*SECTOR_SIZE + rs*SECTOR_SIZE;

		nibblize_sector(DSK_VOLUME, track_index as u8, sector as u8,
				&buffer.as_slice()[sector_start..sector_start+SECTOR_SIZE],
				&mut bit_writer);
	    }

	    // Flushes output stream to writer, if necessary. Any partial bytes are not flushed.
	    bit_writer.flush().expect("");

	    // Consumes writer and returns any un-written partial byte as a (bits, value) tuple.
	    let (last_cnt, last_bits) = bit_writer.into_unwritten();

	    if track_index == 0 {
		let v = track_bits.as_slice();
		debug!("[{}]", v.iter().take(378).fold(
		    String::new(), |acc, &num| acc + format!("{:02X}",num).as_str() + ", "));
	    }

	    // Notice that since we write 16 sectors
	    // then the number of bits in a track is always a multiple
	    // of 16. So last_cnt is always == 0
	    // Anyway, I prefer leave this code here as it
	    // sounds more future proof...

	    let mut nb_bits = track_bits.len()*8;
	    if last_cnt > 0 {
		track_bits.push(last_bits);
		nb_bits += last_cnt as usize;
	    }

	    // Now add the track to the index (repeating to have quarter tracks)

	    woz_file.sub_track_index.push(track_index);
	    woz_file.sub_track_index.push(track_index);
	    woz_file.sub_track_index.push(EMPTY_SUB_TRACK);
	    if track_index < TRACKS_PER_FLOPPY - 1 {
		woz_file.sub_track_index.push(track_index + 1);
	    } else {
		woz_file.sub_track_index.push(EMPTY_SUB_TRACK);
	    }

	    debug!("DSK import: Track: {:03}, {} bytes, {} bits (+{})",
		   track_index,
		   track_bits.len(),
		   nb_bits,
		   last_cnt);

	    woz_file.track_data.push(TrackData::new(track_bits, nb_bits));
	}

	woz_file
    }
}


pub fn load_disk(file_path: &Path) -> WozFile {
    let f = File::open(file_path).expect("open");
    let mut buffer:Vec<u8> = Vec::new();
    f.take(4).read_to_end(&mut buffer).expect("read");

    let woz_ident = str::from_utf8(&buffer[0..4]);

    let woz = if woz_ident.is_err() {
	WozFile::read_dsk(file_path)
    } else {
	match woz_ident.unwrap() {
	    "WOZ1" => WozFile::read(file_path),
	    "WOZ2" => WozFile::read(file_path),
	    _ => WozFile::read_dsk(file_path)
	}
    };

    // let path: &Path = Path::new("track0.bin");
    // use std::fs;
    // fs::write(path, woz.track_data[0].track_bits.as_slice()).unwrap();

    return woz
}


#[cfg(test)]
mod tests {
    use super::*;

    //let mut controller = Controller:new();

    fn read_nibble(mut cycle: u64, controller: &mut Controller) -> (u64, u8) {
	let mut limit = 64;

	let mut b = controller.data_register(cycle);
	cycle += 1;

	// Finish current nibble
	while b & 0x80 > 0 {
	    b = controller.data_register(cycle);
	    cycle += 1;

	    limit -= 1;
	    if limit == 0 {
		assert!(false, "Limit exhausted");
	    }
	};

	// Wait for next nibble
	while b & 0x80 == 0 {
	    b = controller.data_register(cycle);
	    cycle += 1;

	    limit -= 1;
	    if limit == 0 {
		assert!(false, "Limit exhausted");
	    }
	}

	// Read new nibble
	let r = b;
	trace!("NIBBLE! Cycle:{:6} ${:02X}", cycle, b);
	while b & 0x80 > 0 {
	    b = controller.data_register(cycle);
	    cycle += 1;

	    limit -= 1;
	    if limit == 0 {
		assert!(false, "Limit exhausted");
	    }
	};
	(cycle, r)
    }

    fn read_to_sector(mut cycle: u64, controller: &mut Controller) -> (u64, u8, u8) {
	loop {
	    let (c,n) = read_nibble(cycle, controller);
	    cycle = c;
	    if n == 0xD5 {
		let (c,n) = read_nibble(cycle, controller);
		cycle = c;
		if n == 0xAA {
		    let (c,n) = read_nibble(cycle, controller);
		    cycle = c;
		    if n == 0x96 {

			/*    let address_block: [u8; 8] = [
			((vol >> 1) | 0xaa), (vol | 0xaa),
			((trk >> 1) | 0xaa), (trk | 0xaa),
			((sector >> 1) | 0xaa), (sector | 0xaa),
			((chksum >> 1) | 0xaa), (chksum | 0xaa)
		    ];
			 */

			fn decode(b1: u8, b2: u8) -> u8 {
			    ((b1 & (!0xAA)) << 1) | b2 & (!0xAA)
			}

			let (cycle, vol1) = read_nibble(cycle, controller);
			let (cycle, vol2) = read_nibble(cycle, controller);
			let (cycle, trk1) = read_nibble(cycle, controller);
			let (cycle, trk2) = read_nibble(cycle, controller);
			let (cycle, sec1) = read_nibble(cycle, controller);
			let (cycle, sec2) = read_nibble(cycle, controller);

			return (cycle, decode(trk1, trk2), decode(sec1, sec2))
		    }
		}
	    }
	}
    }

    #[test]
    fn small_track_change() {
	let mut c = Controller::new();
	c.woz_file = load_disk(Path::new("conan.dsk"));

	for i in 0..10 {
	    println!("{}", c.woz_file.sub_track_index[i]);
	}

	c.current_subtrack = 3;
	let mut cycle = 1;
	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
	assert!(c.current_subtrack == 3);

	c.begin_head_move(cycle, 1);
	cycle += 1;
	c.end_head_move();
	cycle += 1;

	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
	assert!(c.current_subtrack == 4);

	c.begin_head_move(cycle, -1);
	cycle += 1;
	c.end_head_move();
	cycle += 1;

	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
	assert!(c.current_subtrack == 3);
    }


    #[test]
    fn track_change_limits() {
	let mut c = Controller::new();
	c.woz_file = load_disk(Path::new("LOWTECH.WOZ"));
	c.current_subtrack = 1;
	let mut cycle = 1;

	for i in 1..(37*4) {
	    c.begin_head_move(cycle, 1);
	    cycle += 1;
	    c.end_head_move();
	    cycle += 1;

	    if !c.is_current_subtrack_empty() {
		let (c2, trk, sect) = read_to_sector(cycle, &mut c);
		cycle = c2;
		println!("i:{} Trk:{}, Sect:{}", i, trk, sect);
	    }
	}

	// 36*4 = 144
	assert!(c.current_subtrack == DRIVE_SUB_TRACKS-1, "bad current subtrack {}", c.current_subtrack);


	for i in 1..(40*4) {
	    c.begin_head_move(cycle, -1);
	    cycle += 1;
	    c.end_head_move();
	    cycle += 1;
	    if !c.is_current_subtrack_empty() {
		let (c2, trk, sect) = read_to_sector(cycle, &mut c);
		cycle = c2;
	    }
	}

	assert!(c.current_subtrack == 0);
    }


    #[test]
    fn read_test() {
	let mut log_builder = &mut env_logger::builder();
	log_builder = log_builder
	    .format_timestamp(None)
	    .filter(None, LevelFilter::Debug);
	log_builder.init();


	let mut c = Controller::new();
	c.woz_file = load_disk(Path::new("conan.dsk"));
	c.current_subtrack = 1;

	let mut cycle = 1;
	let mut bytes_read: Vec<u8> = Vec::new();
	for i in 1..17 {
	    let (c2, b) = read_nibble(cycle, &mut c);
	    cycle = c2;
	    bytes_read.push(b);
	}

	for i in 0..bytes_read.len() {
	    println!("{} {:02X}", i, bytes_read[i]);
	}
	assert!(bytes_read.as_slice()[12..=14] == [0xD5,0xAA,0x96]);

	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

	cycle += 12000;
	c.data_register(cycle);
	cycle += 1;
	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

	cycle += 220_000;
	c.data_register(cycle);
	cycle += 1;
	let (c2, trk, sect) = read_to_sector(cycle, &mut c);
	cycle = c2;
	println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

	// for i in 1..202_230 {
	//     let b = c.data_register(cycle);

	//     if b & 0x80 > 0 {
	// 	println!("{:3} {:02X}", cycle, b)
	//     }
	//     cycle += 1;
	// }
    }
}
